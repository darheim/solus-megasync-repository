Solus unofficial MEGAsync repository
====================================

This is an unofficial Solus repository for the MEGAsync software, not included in the official Solus repositories due to legal issues. It is a continuation of [Devil505's](https://gitlab.com/devil505/solus-3rd-party-repo) and [abdulocracy's](https://gitlab.com/abdulocracy/solus-megasync-repository) efforts.

*These packages are not official, so not reviewed or supported by the Solus team. Do not post on official Solus channels.*

## Adding the repository (or migrating from the old repository)

`sudo eopkg add-repo megasync https://gitlab.com/darheim/solus-megasync-repository/raw/master/packages/eopkg-index.xml.xz`

## Removing the repository

`sudo eopkg remove-repo megasync`

## Package list

| Package           | Task                                   | Status   |
| ----------------- | -------------------------------------- | -------- |
| megasync          | [Task 163](https://dev.getsol.us/T163) | Rejected |
| nautilus-megasync |

## Problem?

Please file a report [here](https://gitlab.com/darheim/solus-megasync-repository/issues).

Again, **don't** post on official Solus channels.
